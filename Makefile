CC=gcc
CFLAGS=-Wall -Wextra -ggdb3 -O0
.PHONY: clean

all: main

main: main.o
	$(CC) main.o -o main

main.o: main.c
	$(CC) $(CFLAGS) -c main.c -o main.o

clean:
	rm -f ./main ./main.o

