#include <arpa/inet.h>
#include <asm/types.h>
#include <ctype.h>
#include <errno.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <net/if.h>
#include <net/route.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>

#define BUFSIZE 0x1000

void
dump(uint8_t* data, size_t len);

int
handle_rta(struct rtattr* rt);

int
main(void)
{
  struct nlmsghdr* nlms = NULL;
  struct rtmsg* rtms = NULL;
  struct rtattr* rta = NULL;
  struct sockaddr_nl snl;
  struct iovec iov;
  uint8_t buffer[BUFSIZE];
  struct msghdr msg;
  ssize_t recvsize;
  ssize_t rtasize;

  memset(buffer, 0x00, BUFSIZE);
  memset(&msg, 0x00, sizeof msg);
  memset(&iov, 0x00, sizeof iov);
  memset(&snl, 0x00, sizeof snl);

  int sofd = socket(AF_NETLINK, SOCK_RAW | SOCK_CLOEXEC, NETLINK_ROUTE);
  if (sofd < 0) {
    perror("socket");
    exit(1);
  }

  snl.nl_family = AF_NETLINK;
  snl.nl_groups = 0;
  snl.nl_pid = 0;

  if (bind(sofd, (const struct sockaddr*)&snl, sizeof snl) < 0) {
    perror("bind");
    exit(1);
  }

  nlms = (struct nlmsghdr*)buffer;
  // XXX THis should be correct because
  // NLMSG_LENGTH(len) ((len) + NLMSG_HDRLEN)
  nlms->nlmsg_len = NLMSG_LENGTH(sizeof(struct rtmsg*));
  // Also look at NLM_F_DUMP
  // nlms->nlmsg_flags = NLM_F_REQUEST;
  nlms->nlmsg_flags = NLM_F_REQUEST | NLM_F_DUMP;
  // when NLM_F_ACK is set  - An acknowledgment is an NLMSG_ERROR packet with
  // the error field set to 0
  // struct nlmsgerr
  nlms->nlmsg_type = RTM_GETROUTE;
  nlms->nlmsg_seq = rand();
  nlms->nlmsg_pid = getpid();

  // rtms = (struct rtmsg*)(nlms + 1);
  rtms = (struct rtmsg*)NLMSG_DATA(nlms);
  rtms->rtm_family = AF_INET; // Should implement AF_INET6 also...
  rtms->rtm_dst_len = 0;
  rtms->rtm_src_len = 0;
  // rtms->rtm_table = RT_TABLE_MAIN;

  // rta = RTM_RTA(rtms);
  // rta->rta_type = RTA_DST;
  // rta->rta_len = RTA_LENGTH(32);
  // inet_pton(AF_INET, "1.1.1.1", RTA_DATA(rta));

  // rta = RTM_RTA(rtms);
  // rta->rta_type = RTA_TABLE;
  // rta->rta_len = RTA_LENGTH(2);
  //*(int*)RTA_DATA(rta) = 10;

  // rta = RTM_RTA(rtms);
  // rta->rta_type = RTA_PREFSRC;
  // rta->rta_len = RTA_LENGTH(32);
  // inet_pton(AF_INET, "10.0.0.7", RTA_DATA(rta));

  iov.iov_base = nlms;
  iov.iov_len = nlms->nlmsg_len;

  msg.msg_name = &snl;
  msg.msg_namelen = sizeof snl;
  msg.msg_iov = &iov;
  msg.msg_iovlen = 1;

  if (sendmsg(sofd, &msg, 0) < 0) {
    perror("sendmsg");
    exit(1);
  }

  iov.iov_base = buffer;
  iov.iov_len = sizeof buffer;

  if ((recvsize = recvmsg(sofd, &msg, 0)) <= 0) {
    perror("recvmsg");
    exit(1);
  }
  for (nlms = (struct nlmsghdr*)buffer; NLMSG_OK(nlms, recvsize);
       nlms = NLMSG_NEXT(nlms, recvsize)) {

    if (nlms->nlmsg_type == NLMSG_ERROR) {
      perror("NLMSG_ERROR");
      return (0);
    }
    if (nlms->nlmsg_type == NLMSG_DONE) {
      printf("Received everything\n");
      break;
    }

    /*printf(*/
    /*"recvmsg %zi nlms->nlmsg_len %zi\n", recvsize,
     * (ssize_t)nlms->nlmsg_len);*/

    rtms = (struct rtmsg*)NLMSG_DATA(nlms);

    if (rtms->rtm_table != RT_TABLE_MAIN) {
      continue;
    }

    rtasize = nlms->nlmsg_len;
    // rta = RTM_RTA((nlms + 1)); /* RTM_ works on struct rtmsg, that's why +1*/
    // rta = RTM_RTA(rtms); /* RTM_ works on struct rtmsg, */
    //
    //    while (RTA_OK(rta, rtasize)) {
    //      handle_rta(rta);
    //      rta = RTA_NEXT(rta, rtasize);
    //    }

    for (rta = RTM_RTA(rtms); RTA_OK(rta, rtasize);
         rta = RTA_NEXT(rta, rtasize)) {
      handle_rta(rta);
    }

    printf("\n---------------------------\n");
  }

  return 0;
}

void
dump(uint8_t* data, size_t len)
{
  for (size_t i = 0; i < len; i++) {
    printf("%#X ", data[i]);
  }
  printf("\n\n");
}

// TODO Add support for AF_INET6
int
handle_rta(struct rtattr* rt)
{
  char buffer[20] = { 0 };
  int sofd = -1;
  struct ifreq hwinfo;
  memset(&hwinfo, 0x00, sizeof hwinfo);
  if (!rt) {
    return 0;
  }
  switch (rt->rta_type) {
    case RTA_DST: {
      inet_ntop(AF_INET, RTA_DATA(rt), buffer, sizeof buffer);
      printf("-- destination: %s ", buffer);
      break;
    }
    case RTA_SRC: {
      inet_ntop(AF_INET, RTA_DATA(rt), buffer, sizeof buffer);
      printf("-- source: %s ", buffer);
      break;
    }
    case RTA_PREFSRC: {
      inet_ntop(AF_INET, RTA_DATA(rt), buffer, sizeof buffer);
      printf("-- prefsource: %s ", buffer);
      break;
    }
    case RTA_GATEWAY: {
      inet_ntop(AF_INET, RTA_DATA(rt), buffer, sizeof buffer);
      printf("-- gateway: %s ", buffer);
      break;
    }
    case RTA_PRIORITY: {
      printf("-- metric: %d ", *(int*)RTA_DATA(rt));
      break;
    }
    case RTA_OIF: {
      sofd = socket(AF_UNIX, SOCK_DGRAM | SOCK_CLOEXEC, 0);
      if (sofd < 0) {
        break;
      }
      hwinfo.ifr_ifru.ifru_ivalue = *(int*)RTA_DATA(rt);
      if (ioctl(sofd, SIOCGIFNAME, &hwinfo) != -1) {
        printf("-- ifname %s ", hwinfo.ifr_ifrn.ifrn_name);
      }
      break;
    }
    default: {
      break;
    }
  }
  return 1;
}
